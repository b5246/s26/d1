let http = require("http");

// Creates a variable "port" to store the port number
const port = 4000;

const server = http.createServer((request,response)=>{
	if(request.url == '/greetings'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('You are in /greetings endpoint');
	} else if(request.url == '/homepage'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('You are in /homepage endpoint');
	} else {
		response.writeHead(404,{'Content-Type':'text/plain'});
		response.end('Page not available');
	}
});
// ref for => and backticks: s19/d1/index.js lecture

server.listen(port);

console.log(`Server now accessible at localhost:4000`);

/*
TO BROWSER URL: http://localhost:4000/greetings
*/



