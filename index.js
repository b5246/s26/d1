//Requiring the HTTP module
let http =  require("http");    // 'require'-  lets node.js to transfer data via HTTP

// Creating the server
// createServer() method

http.createServer(function(request, response) {
	response.writeHead(200, {'Content-Type': 'text/plain'}); // 'status code : 404' & 200 == OK
	response.end('Hello World Batch 177'); // <=== Message
}).listen(4000);

// Create console log to indicate that the srver is running
console.log('Server running at localhost: 4000');

/*
TO BROWSER URL: http://localhost:4000/
*/






















